#!/usr/bin/env python

import pytest
from datetime import datetime
from .pages.signup import SignupPage
from .pages.users import UsersPage


@pytest.mark.usefixtures('signup')
class TestSignup(object):
    # set timestamp to nearest second
    TIMESTAMP = round(datetime.now().timestamp())
    USERNAME = 'user{timestamp}'.format(timestamp=TIMESTAMP)
    EMAIL = 'user{timestamp}@example.com'.format(timestamp=TIMESTAMP)
    PASSWORD = 'password'
    EXPECTED_BANNER_TEXT = 'Welcome to the alpha blog user{timestamp}'.format(timestamp=TIMESTAMP)

    def test_signup(self):
        signup = SignupPage(self.driver)
        signup.enter_username(self.USERNAME)
        signup.enter_email(self.EMAIL)
        signup.enter_password(self.PASSWORD)
        signup.submit_form()

        users = UsersPage(self.driver)
        banner_text = users.get_banner_text()

        assert banner_text == self.EXPECTED_BANNER_TEXT


# this isn't working: "ModuleNotFoundError: No module named '__main__.pages'; '__main__' is not a package"
if __name__ == '__main__':
    test = TestSignup()
    test.test_signup()
