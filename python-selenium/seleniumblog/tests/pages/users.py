#!/usr/bin/env python


class UsersPage(object):
    # Locators
    SUCCESS_BANNER = 'flash_success'

    def __init__(self, driver):
        self.driver = driver

    def get_banner_text(self):
        banner = self.driver.find_element_by_id(self.SUCCESS_BANNER)
        return banner.text
