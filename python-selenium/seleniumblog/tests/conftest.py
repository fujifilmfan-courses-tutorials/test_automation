#!/usr/bin/env python

import pytest
from selenium import webdriver


@pytest.fixture(scope='class')
def signup(request):
    driver = webdriver.Chrome()
    driver.get('https://selenium-blog.herokuapp.com/signup')
    # driver.maximize_window()
    request.cls.driver = driver

    yield driver
    driver.close()
