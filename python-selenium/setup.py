#!/usr/bin/env python

# run from python-selenium/ : $ pip install -e
from setuptools import setup, find_packages

setup(name='seleniumblog', version='1.0', packages=find_packages())
setup(name='theinternet', version='0.1', packages=find_packages())
