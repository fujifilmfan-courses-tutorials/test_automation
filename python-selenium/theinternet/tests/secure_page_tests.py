#!/usr/bin/env python

import pytest
from .pages.login import LoginPage
from .pages.secure import SecurePage


@pytest.mark.usefixtures('theinternet')
class TestSecure(object):
    # input variables, sorted alphabetically
    password = 'SuperSecretPassword!'
    username = 'tomsmith'

    # output variables, sorted alphabetically
    expected_secure_header_text = 'Secure Area'
    expected_success_banner = 'You logged out of the secure area!\n×'
    expected_success_header_text = 'Login Page'

    def attempt_login(self, username, password):
        self.login = LoginPage(self.driver)
        self.driver.get(self.login.url)
        self.login.enter_username(username)
        self.login.enter_password(password)
        self.login.submit_form()

    def test_logout_button(self):
        self.attempt_login(self.username, self.password)

        users = SecurePage(self.driver)
        users.logout()
        
        login = LoginPage(self.driver)
        banner_text = login.get_banner_text()
        header_text = login.get_header_text()

        assert banner_text == self.expected_success_banner
        assert header_text == self.expected_success_header_text

    def test_close_banner(self):
        self.attempt_login(self.username, self.password)

        users = SecurePage(self.driver)
        users.close_banner()

        assert users.expect_missing_element('success_missing_banner') is True
