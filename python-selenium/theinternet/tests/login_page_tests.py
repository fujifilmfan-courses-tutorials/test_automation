#!/usr/bin/env python

import pytest
from .pages.login import LoginPage
from .pages.secure import SecurePage


@pytest.mark.usefixtures('theinternet')
class TestLogin(object):
    # input variables, sorted alphabetically
    invalid_password = 'SuperSecretPassword'
    invalid_username = 'tomsmit'
    password = 'SuperSecretPassword!'
    username = 'tomsmith'

    # output variables, sorted alphabetically
    expected_login_header_text = 'Login Page'
    expected_password_error = 'Your password is invalid!\n×'
    expected_success_banner = 'You logged into a secure area!\n×'
    expected_success_header_text = 'Secure Area'
    expected_username_error = 'Your username is invalid!\n×'

    def attempt_login(self, username, password):
        self.login = LoginPage(self.driver)
        
        # see comment in conftest.py, login() for an explanation of this decision
        self.driver.get(self.login.url)
        self.login.enter_username(username)
        self.login.enter_password(password)
        self.login.submit_form()

    def test_login_valid_credentials(self):
        self.attempt_login(self.username, self.password)

        users = SecurePage(self.driver)
        banner_text = users.get_banner_text()
        header_text = users.get_header_text()

        assert banner_text == self.expected_success_banner
        assert header_text == self.expected_success_header_text

    def test_login_invalid_username(self):
        self.attempt_login(self.invalid_username, self.password)

        banner_text = self.login.get_error_text()
        header_text = self.login.get_header_text()

        assert banner_text == self.expected_username_error
        assert header_text == self.expected_login_header_text

    def test_login_invalid_password(self):
        self.attempt_login(self.username, self.invalid_password)

        banner_text = self.login.get_error_text()
        header_text = self.login.get_header_text()

        assert banner_text == self.expected_password_error
        assert header_text == self.expected_login_header_text
