#!/usr/bin/env python

import pytest
from .pages.alerts import AlertsPage


@pytest.mark.usefixtures('theinternet')
class TestAlerts(object):
    # output variables, sorted alphabetically
    expected_js_alert_text = 'You successfuly clicked an alert'
    expected_js_confirm_text_cancel = 'You clicked: Cancel'
    expected_js_confirm_text_okay = 'You clicked: Ok'
    expected_js_prompt_text_cancel = 'You entered: null'
    expected_js_prompt_text_okay = 'You entered: Hello world'

    def test_javascript_alert(self):
        self.alerts = AlertsPage(self.driver)
        self.driver.get(self.alerts.url)
        self.alerts.click_alert()
        popup = self.driver.switch_to.alert
        popup.accept()

        result = self.alerts.get_result_text()
        assert result == self.expected_js_alert_text

    def test_javascript_confirm_okay(self):
        self.alerts = AlertsPage(self.driver)
        self.driver.get(self.alerts.url)
        self.alerts.click_confirm()
        popup = self.driver.switch_to.alert
        popup.accept()

        result = self.alerts.get_result_text()
        assert result == self.expected_js_confirm_text_okay

    def test_javascript_confirm_cancel(self):
        self.alerts = AlertsPage(self.driver)
        self.driver.get(self.alerts.url)
        self.alerts.click_confirm()
        popup = self.driver.switch_to.alert
        popup.dismiss()

        result = self.alerts.get_result_text()
        assert result == self.expected_js_confirm_text_cancel

    def test_javascript_prompt_okay(self):
        self.alerts = AlertsPage(self.driver)
        self.driver.get(self.alerts.url)
        self.alerts.click_prompt()
        popup = self.driver.switch_to.alert
        popup.send_keys('Hello world')
        popup.accept()

        result = self.alerts.get_result_text()
        assert result == self.expected_js_prompt_text_okay

    def test_javascript_prompt_cancel(self):
        self.alerts = AlertsPage(self.driver)
        self.driver.get(self.alerts.url)
        self.alerts.click_prompt()
        popup = self.driver.switch_to.alert
        # I am intentionally sending keys here; the resulting text should still include 'null'
        popup.send_keys('Hello world')
        popup.dismiss()

        result = self.alerts.get_result_text()
        assert result == self.expected_js_prompt_text_cancel
