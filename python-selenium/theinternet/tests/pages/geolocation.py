#!/usr/bin/env python

import pytest
from selenium.common.exceptions import NoSuchElementException


class GeolocationPage(object):
    url = 'https://the-internet.herokuapp.com/geolocation'

    # Locators sorted alphabetically
    button = '/html/body/div[2]/div/div/button'
    content = '#content'
    google_link = '#map-link'
    latitude = '#lat-value'
    longitude = '#long-value'

    def __init__(self, driver):
        self.driver = driver

    def click_button(self):
        button = self.driver.find_element_by_xpath(self.button)
        button.click()

    def element_does_not_exist(self, element):
        with pytest.raises(NoSuchElementException):
            self.driver.find_element_by_css_selector(element)

    def get_latitude(self):
        latitude = self.driver.find_element_by_css_selector(self.latitude)
        return latitude.text

    def get_longitude(self):
        longitude = self.driver.find_element_by_css_selector(self.longitude)
        return longitude.text

    def get_google_link_text(self):
        google_link = self.driver.find_element_by_css_selector(self.google_link)
        return google_link.text
