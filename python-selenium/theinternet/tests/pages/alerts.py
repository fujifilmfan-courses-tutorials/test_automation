#!/usr/bin/env python


class AlertsPage(object):
    url = 'https://the-internet.herokuapp.com/javascript_alerts'

    # Locators sorted alphabetically
    # js_alert = "//div[@id='content']/div[@class='example']/ul/li[text() = 'Click for JS Alert']"
    # js_alert = ".//button[contains(text(), 'Click for JS Alert')]"
    js_alert = "//div/ul/li[1]/button"
    js_confirm = "//div/ul/li[2]/button"
    js_prompt = "//div/ul/li[3]/button"
    result = '#result'

    def __init__(self, driver):
        self.driver = driver

    def click_alert(self):
        button = self.driver.find_element_by_xpath(self.js_alert)
        button.click()

    def click_confirm(self):
        button = self.driver.find_element_by_xpath(self.js_confirm)
        button.click()

    def click_prompt(self):
        button = self.driver.find_element_by_xpath(self.js_prompt)
        button.click()

    def get_result_text(self):
        result = self.driver.find_element_by_css_selector(self.result).text
        return result
