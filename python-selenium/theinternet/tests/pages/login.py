#!/usr/bin/env python


class LoginPage(object):
    url = 'https://the-internet.herokuapp.com/login'
    
    # Locators sorted alphabetically
    error_banner = '#flash'
    header = '#content > .example > h2'
    login_button = '.radius'
    password_field = '#password'
    success_banner = '#flash'
    success_banner_close = '#close'
    username_field = '#username'

    def __init__(self, driver):
        self.driver = driver

    def close_banner(self):
        close = self.driver.find_element_by_css_selector(self.success_banner_close)
        close.click()

    def enter_username(self, username):
        username_field = self.driver.find_element_by_css_selector(self.username_field)
        username_field.send_keys(username)

    def enter_password(self, password):
        password_field = self.driver.find_element_by_css_selector(self.password_field)
        password_field.send_keys(password)

    def get_banner_text(self):
        banner = self.driver.find_element_by_css_selector(self.success_banner)
        return banner.text
        
    def get_error_text(self):
        error = self.driver.find_element_by_css_selector(self.error_banner)
        return error.text

    def get_header_text(self):
        header = self.driver.find_element_by_css_selector(self.header)
        return header.text

    def submit_form(self):
        submit_button = self.driver.find_element_by_css_selector(self.login_button)
        submit_button.click()
