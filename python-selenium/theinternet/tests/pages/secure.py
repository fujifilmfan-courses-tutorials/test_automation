#!/usr/bin/env python

from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.keys import Keys

class SecurePage(object):
    # Locators sorted alphabetically
    header = '#content > .example > h2'
    logout_button = '.button'
    success_banner = '#flash'
    success_banner_close = '.close'

    def __init__(self, driver):
        self.driver = driver

    def close_banner(self):
        close = self.driver.find_element_by_css_selector(self.success_banner_close)
        # `close.click()` does not work with Chrome, but it does with Firefox.
        # `send_keys()` works with both.
        # Reference: https://stackoverflow.com/questions/11908249/debugging-element-is-not-clickable-at-point-error
        close.send_keys(Keys.RETURN)

    def expect_missing_element(self, element):
        missing_element = 'self.{element}'.format(element=element)
        try:
            self.driver.find_element_by_css_selector(missing_element)
            return False
        except NoSuchElementException:
            return True

    def get_banner_text(self):
        banner = self.driver.find_element_by_css_selector(self.success_banner)
        return banner.text
    
    def get_header_text(self):
        header = self.driver.find_element_by_css_selector(self.header)
        return header.text

    def logout(self):
        button = self.driver.find_element_by_css_selector(self.logout_button)
        button.click()
