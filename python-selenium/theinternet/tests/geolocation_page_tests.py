#!/usr/bin/env python

import pytest
from .pages.geolocation import GeolocationPage


@pytest.mark.usefixtures('theinternet')
class TestGeolocation(object):
    # output variables, sorted alphabetically
    expected_google_link_text = 'See it on Google'

    def test_lat_long_not_displayed_on_page_load(self):
        self.geo = GeolocationPage(self.driver)
        self.driver.get(self.geo.url)

        # The next line causes a test failure, as #content does exist on the page at page load
        # self.geo.element_does_not_exist(self.geo.content)
        self.geo.element_does_not_exist(self.geo.latitude)
        self.geo.element_does_not_exist(self.geo.longitude)

    def test_clicking_button_results_in_lat_long_displayed(self):
        self.geo = GeolocationPage(self.driver)
        self.driver.get(self.geo.url)
        # Perhaps switch to an explicit wait using the text_to_be_present_in_element condition
        self.driver.implicitly_wait(2)
        self.geo.click_button()

        latitude = self.geo.get_latitude()
        longitude = self.geo.get_longitude()

        assert len(latitude) > 0
        assert len(longitude) > 0

        print('\nlatitude: ' + latitude)
        print('longitude: ' + longitude)

    def test_clicking_button_results_in_google_link(self):
        self.geo = GeolocationPage(self.driver)
        self.driver.get(self.geo.url)
        # Perhaps switch to an explicit wait using the text_to_be_present_in_element condition
        self.driver.implicitly_wait(2)
        self.geo.click_button()

        google_link_text = self.geo.get_google_link_text()

        assert google_link_text == self.expected_google_link_text
