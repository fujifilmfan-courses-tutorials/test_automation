#!/usr/bin/env python

import pytest
from selenium import webdriver


@pytest.fixture(scope='class')
def theinternet(request):
    driver = webdriver.Chrome()
    """ Since a successful login leaves the browser on a different page, subsequent tests fail.
    Therefore, I moved the .get to the attempt_login() method in the TestLogin class.  Another
    option would be to execute a logout after a successful login, but that seems to add
    unnecessary dependencies on other functions.
    """
    # driver.get('https://the-internet.herokuapp.com/login')
    request.cls.driver = driver

    yield driver
    driver.close()
