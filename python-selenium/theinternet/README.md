the-internet.herokuapp.com Tests
--------------------------------

### Overview
----
#### Design choices

I started by trying to implement a pytest framework using fixtures, but having no previous experience with that, I couldn't get it off the ground within a reasonable time frame.  
I then briefly attempted a Robot Framework implementation, but as I recalled the details of the Robot implementation we used at OpenX, I realized that that, too, would take too long to build from the ground up.  
I learned way more than I can document here about the myriad ways to implement a POM in different frameworks.  

In the end, I settled for a simple, script-based implementation that started with the basic structure of a previous [Ruby project](https://gitlab.com/fujifilmfan/test_automation/tree/master/ruby-selenium), which suited itself well for pytest fixtures.  
I could have settled for using unittest, but I preferred to use native Python asserts.  


### Testing notes
----
#### Login page

* "Write all tests you think necessary for the following form: https://the-internet.herokuapp.com/login"  
   * I wrote three tests for basic permutations of login credentials, and I added some asserts related to page content.  
   * Assertions related to page content (like headers) are not always necessary, but I added them for demonstration purposes.  
   * I could make a separate set of tests for various page elements, but given time constraints, these are not important to test in UI tests.  
   
#### JavaScript alerts
* "Write tests for all three javascript alerts: https://the-internet.herokuapp.com/javascript_alerts"  
   * I lost time trying to 'fix' my XPaths at the beginning when all along I wasn't loading the page. :(
   
#### Dynamic controls  
* "Validate dynamic controls - enable/disable input field and checkbox: https://the-internet.herokuapp.com/dynamic_controls"  
   * I don't think I can do this in 20 minutes, so I'm skipping it for now.
   
#### Lat and long
* "Write a test to check the current latitude and longitude: https://the-internet.herokuapp.com/geolocation"  
   * I get different geos each time I click "Where am I?" so it seems clear that I either need to round the values or else get the geos from the browser to confirm the text on page.  
   * The function getLocation() is defined on-page:
    ```  
      var x=document.getElementById("demo");
      function getLocation()
        {
        if (navigator.geolocation)
          {
          navigator.geolocation.getCurrentPosition(showPosition);
          }
        else{x.innerHTML="Geolocation is not supported by this browser.";}
        }
      function showPosition(position)
        {
        x.innerHTML="Latitude: <div id='lat-value'>" + position.coords.latitude +
        "</div><br>Longitude: <div id='long-value'>" + position.coords.longitude +
        "</div><br><div id='map-link'><a href='http://maps.google.com/?q=" + position.coords.latitude +
        "," + position.coords.longitude + "'>See it on Google</a></div>";
        }
    ```  
 
   * If I can't validate the geolocation independently, I can at least return the values.  
   * Still do to: verify that clicking the link opens Google Maps  

### Running tests
----
From the `tests/` directory, run a test file with pytest: `$ pytest login_page_tests.py secure_page_tests.py 
alerts_page_tests.py geolocation_page_test.py`  
  
**Passing tests for login page, secure area, and javascript alerts**  
![Passing tests](images/Screen Shot 2019-03-11 at 16.19.02.png)
