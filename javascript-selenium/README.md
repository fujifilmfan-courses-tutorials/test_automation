Stickerfy
---------

The Stickerfy application is a simple eCommerce app built with **Node.js**, **Express.js**, **MongoDB**, and **Mongoose**.  **Mocha**, **Chai HTTP**, **chromedriver**, and **Selenium WebDriver** are used for testing.  It did not work for me "out-of-the-box", so I modified the instructions from https://github.com/meaghanlewis/stickerfy with what worked for me.

### Run the application locally
----
Needed:  
* [Node](https://nodejs.org/en/download/)  
* [NPM](https://www.npmjs.com/get-npm) (which is bundled with Node)  
* [MongoDB](https://docs.mongodb.com/manual/installation/\#mongodb-community-edition)
   * After installing MongoDB (`$ brew install mongodb`), create `/data/db`.  
   * This was helpful: [Unable to create/open lock file: /data/mongod.lock errno:13 Permission denied](https://stackoverflow.com/questions/15229412/unable-to-create-open-lock-file-data-mongod-lock-errno13-permission-denied)  
   * Basically, do `$ sudo mkdir -p /data/db/` then `$ sudo chown USERNAME /data/db`.  
  
To get started:
1. Clone this repository
    `$ git clone https://github.com/meaghanlewis/stickerfy.git`  
  
2. Make some modifications
   * Add `"bluebird": "3.5.3"`, to the `dependencies` in `stickerfy/package.json`.  
   * Add `mongoose.Promise = require('bluebird');` to line 5 of `stickerfy/seed/product-seeder.js`.  
   * Remove the reference to mongoose port 27017 as that is the default.
      * In `stickerfy/seed/product-seeder.js` line 6 (formerly line 5), change `mongoose.connect('mongodb://localhost:27017/shopping', { useMongoClient: true });` to `mongoose.connect('mongodb://localhost/shopping', { useMongoClient: true });`.  
  
3. Install dependencies

   `$ npm install`  
  
4. Start up MongoDB in a separate terminal.

   `$ mongod`  
  
5. Populate shopping database

   `$ node seed/product-seeder.js`  
  
6. Once the project is setup, start the dev server with Nodemon.

   `$ npm run dev`  
   Server will start at: http://localhost:3000  

### Run Mocha tests
----
#### Unit tests
- to run: `npm run unit-test`
- do not require the dev server running
  
Other notes:  
* `beforeEach()` sets up a model for a cart and product; runs before each test  
* `assert.deepEqual` tests for equivalency (even if different objects) rather than equality (use `assert.equal` for the latter)
* I added:  
   * 'adds two happy stickers to the cart'  
   * 'adds one happy and one sad sticker to the cart'  

#### Integration tests
- to run: `npm run integration-test`
- start up the dev server when running
  
Other notes:
* The sample integration test in section 1.1 does not seem to actually test order completion.  In `index.js`, we can see that the route for `/checkout` includes the following code:  
   ```
     if (!req.session.cart) {
       return res.redirect('/shopping-cart');
     }
   ```
   This redirection is exactly what happens in the test.  We see in the test output:  
   GET /checkout 302 7.182 ms - 36  
   GET /shopping-cart 200 25.490 ms - 2151  
   So the 200 response is actually from /shopping-cart, not /checkout.  This is confirmed in res.path: `path: '/shopping-cart'`.  
  
   However, I'm not sure how to add items to the cart before calling the `/checkout` endpoint to prevent the redirection.  
* Helpful: [Chai HTTP](https://www.chaijs.com/plugins/chai-http/)  

#### UI tests
- to run: `npm run ui-test`
- note: dev server must be started by running `npm run dev` beforehand

### Other notes  
----
* The 'adds a sticker to the cart and checks out' clicks the first button.  I wanted to click the second button but had trouble making it available to the click() method.  
   * Here is a spattering of jumbled code that I tried to use:  
   ```
    // let sad_sticker = driver.findElement(By.xpath("//h3[contains(text(),'Sad')]"));
    // console.log(sad_sticker);
    // await driver.findElement(By.className('btn-success'))
    // await driver.findElements(By.className('btn-success'))[1].click();
    // buttons.then(function (elements) {
    //   var pendingHtml = elements.map(function (elem) {
    //     return elem.getInnerHtml();
    //   });
    //   Promise.all(pendingHtml).then(function (allHtml) {
    //     console.log(allHtml);
    //   });
    //   // console.log(pendingHtml[0]);
    //   // elements[1].click();
    // });
    // console.log(buttons[0]);
    // let sad_sticker = bottons
    // await sad_sticker.findElement(By.className('btn-success')).click();
    ```
   * In the end, `var buttons = await driver.findElements(By.className('btn-success'));` followed by `await buttons[1].click();` worked, but I still don't understand why `await driver.findElements(By.className('btn-success'))[1].click();` didn't.
* I abstracted several variables and functions, including the button elements and test cleanup functions.
* Helpful resources:  
   * [Chai HTTP](https://www.chaijs.com/plugins/chai-http/)  
   * [Find child elements with locator chaining](https://seleniumjava.com/2017/12/07/find-child-elements-with-locator-chaining/)  
   * [Using promises](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Using_promises)  
   * [await](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/await)  
   * [Async/await](https://javascript.info/async-await)  
   * [JavaScript Let (and scope)](https://www.w3schools.com/js/js_let.asp)  
   * [function expressions and function declarations](https://stackoverflow.com/questions/336859/var-functionname-function-vs-function-functionname)  
   * [A quick and complete guide to Mocha testing](https://blog.logrocket.com/a-quick-and-complete-guide-to-mocha-testing-d0e0ea09f09d)  
   * [How to test JavaScript with Mocha -- The Basics](https://codeburst.io/how-to-test-javascript-with-mocha-the-basics-80132324752e)  
