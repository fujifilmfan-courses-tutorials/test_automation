const Cart = require('../../models/cart');
const Product = require('../../models/product');
const assert = require('assert');

let cart;
let happy;

describe('Shopping cart', function() {

  describe('shopping cart model', () => {

    beforeEach(() => {
      cart = new Cart({});
      happy = new Product({
        "imagePath": "https://buildahead.com/wp-content/uploads/2017/02/happy-emoji-smaller.png",
        "title": "Happy",
        "description": "Happy",
        "price": 5});
      angry = new Product({
        "imagePath": "https://cdn.shopify.com/s/files/1/1061/1924/products/Very_Angry_Emoji_7f7bb8df-d9dc-4cda-b79f-5453e764d4ea_large.png?v=1480481058",
        "title": "Angry",
        "description": "Angry sticker",
        "price": 4.5});
      sad = new Product({
        "imagePath": "https://cdn.shopify.com/s/files/1/1061/1924/products/Emoji_Icon_-_Sad_Emoji_large.png?v=1513251055",
        "title": "Sad",
        "description": "Sad sticker",
        "price": 7});
    });

    it('adds a happy sticker to the cart', function() {
      cart.add(happy, happy.id);
      assert.equal(cart.totalQty, 1);
      assert.equal(cart.totalPrice, 5);
    });

    it('adds two happy stickers to the cart', function() {
      cart.add(happy, happy.id);
      cart.add(happy, happy.id);
      assert.equal(cart.totalQty, 2);
      assert.equal(cart.totalPrice, 10);
    });

    it('adds one happy and one sad sticker to the cart', function() {
      cart.add(happy, happy.id);
      cart.add(sad, sad.id);
      assert.equal(cart.totalQty, 2);
      assert.equal(cart.totalPrice, 12);
    });
    
    it('removes a sticker from the cart', function() {
      cart.add(happy, happy.id);
      cart.reduceByOne(happy.id);
      assert.deepEqual(cart.items, {});
      assert.equal(cart.totalPrice, 0);
    });

    it('remove all quantities of sticker from the cart', function() {
      cart.add(happy, happy.id);
      cart.add(happy, happy.id);
      cart.removeItem(happy.id);
      assert.deepEqual(cart.items, {});
      assert.equal(cart.totalPrice, 0);
    });

    it('returns an empty array', function() {
      assert.deepEqual(cart.generateArray(),[]);
    });
  });
});
