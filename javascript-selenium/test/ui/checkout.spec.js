require('chromedriver');
const assert = require('assert');
const {Builder, By} = require('selenium-webdriver');

describe('Checkout workflow', function() {
  let driver;
  let baseUrl = 'http://localhost:3000/'
  let happy = 0;
  let sad = 1;
  let angry = 2;
  let addToCart = 'btn-success';
  let goToCartButton = 'cart';
  let goToCheckoutButton = 'checkout';
  function getId(url, path) {
    let id = url.substr((baseUrl + path).length);
    return id
  }
  function emptyCart(id) {
    driver.get(baseUrl + 'remove/' + id);
  }

  before(async function() {
    // Setup driver once for the test suite
    driver = await new Builder().forBrowser('chrome').build();
  });

  beforeEach(async function() {
    // Begin each test case at the baseUrl and find product buttons
    await driver.get(baseUrl);
    return addToCartButtons = await driver.findElements(By.className(addToCart));
  });

  it('adds a happy sticker to the cart and checks out', async function() {
    // Get product ID for later cart clean-up
    let url = await addToCartButtons[happy].getAttribute('href');
    let id = await getId(url, 'add-to-cart/');

    // Add item to cart, go to cart, and checkout
    await addToCartButtons[happy].click();
    await driver.findElement(By.id(goToCartButton)).click();
    await driver.findElement(By.id(goToCheckoutButton)).click();

    let total = await driver.findElement(By.id('total'));
    assert.equal(await total.getText(), 'Total: $5.5');
    // Remove items in cart to clean-up test
    await emptyCart(id);
  });

  it('adds a sad sticker to the cart and checks out', async function() {
    // Get product ID for later cart clean-up
    let url = await addToCartButtons[sad].getAttribute('href');
    let id = await getId(url, 'add-to-cart/');

    // Add item to cart, go to cart, and checkout
    await addToCartButtons[sad].click();
    await driver.findElement(By.id(goToCartButton)).click();
    await driver.findElement(By.id(goToCheckoutButton)).click();

    let total = await driver.findElement(By.id('total'));
    assert.equal(await total.getText(), 'Total: $7');
    // Remove items in cart to clean-up test
    await emptyCart(id);
  });

  after(() => driver && driver.quit());

});
