require "rspec"
require "selenium-webdriver"
require_relative "signup_page.rb"
require_relative "users_page.rb"

used_username = "user"
used_email = "user@example.com"
password = "somepassword"
success_banner_text = "Welcome to the alpha blog user"
username_error_text = "Username has already been taken"
email_error_text = "Email has already been taken"
username_email_error_text = "Username has already been taken\nEmail has already been taken"

# Create user account for blog
describe "Blog application" do
    describe "New user signup" do
        it "Confirm that a user can successfully signup" do
            # Time.now_to_i represents the epoch
            timestamp = Time.now.to_i
            @driver = Selenium::WebDriver.for :chrome
            @driver.navigate.to "https://selenium-blog.herokuapp.com/signup"
            
            signup = SignupPage.new(@driver)

            # Fill and submit form
            unique_username = signup.unique_username(used_username, timestamp)
            unique_email = signup.unique_email(used_email, timestamp)

            signup.enter_username(unique_username)
            signup.enter_email(unique_email)
            signup.enter_password(password)
            signup.submit_form()

            # Confirm user signup
            users = UsersPage.new(@driver)
            banner_text = users.get_banner_text()

            # rspec method
            expect(banner_text).to eq(users.unique_success_text(success_banner_text, timestamp))

            @driver.quit
        end

        it "Confirm that user account cannot be created with an existing username and email" do
            timestamp = Time.now.to_i
            @driver = Selenium::WebDriver.for :chrome
            @driver.navigate.to "https://selenium-blog.herokuapp.com/signup"

            signup = SignupPage.new(@driver)

            # Fill and submit form
            unique_username = signup.unique_username(used_username, timestamp)
            unique_email = signup.unique_email(used_email, timestamp)

            signup.enter_username(unique_username)
            signup.enter_email(used_email)
            signup.enter_password(password)
            signup.submit_form()

            # Confirm error message
            users = UsersPage.new(@driver)
            error_text = users.get_error_text()

            expect(error_text).to eq(email_error_text)

            @driver.quit
        end
    end
end