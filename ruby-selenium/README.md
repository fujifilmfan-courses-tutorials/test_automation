Selenium Blog
-------------

The [Selenium Blog](https://selenium-blog.herokuapp.com) is a simple Heroku application created for practicing test automation with Selenium.  The tests are written in **Ruby** using **Selenium WebDriver** and **RSpec**.

### Setup the test environment
----
#### Needed:  
* [selenium-webdriver](https://rubygems.org/gems/selenium-webdriver)
* [rspec](https://rubygems.org/gems/rspec)  
* [chromedriver](http://chromedriver.chromium.org/downloads) or  
* [geckodriver](https://github.com/mozilla/geckodriver/releases)  
  
#### To get started:
1. Add dependencies to the Gemfile and install them:
    ```  
    source 'https://rubygems.org'
        gem 'rspec', '~> 3.8'
        gem 'selenium-webdriver', '~> 3.141'
    ```  
   * `$ gem install bundler`  
   * `$ bundle install`  
   * `$ bundle info selenium-webdriver` (optional)  

2. Add requirements to the test file:
   * `require "rspec"`  
   * `require "selenium-webdriver"`  
  
3. Install drivers
   * put the chromedriver and / or geckodriver in $PATH
  
### Run tests
----
#### Running tests
* `$ rspec blog-test.rb`  

### Other notes
----
* To get a particular error message, one can add `> li:nth-of-type(1)` to the locator.  
* I moved methods to create unique, timestamp-related variables to the respective page objects (not sure if that's a good idea) so that I could use unique timestamps for each test in the suite (another option would be to use different suites).  