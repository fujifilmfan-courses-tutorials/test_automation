class UsersPage
    
    # CSS selectors
    SUCCESS_BANNER = {id: 'flash_success'}
    ERROR_BANNER = {css: "html > body > div.container > div > div > div > div > div > ul"}

    # attr reader
    attr_reader :driver
    
    # class methods
    def initialize(driver)
        @driver = driver
    end

    def unique_success_text(success_banner_text, timestamp)
        success_banner_text = "Welcome to the alpha blog user#{timestamp}"
    end

    def get_banner_text()
        banner = driver.find_element(SUCCESS_BANNER)
        banner_text = banner.text
    end

    def get_error_text()
        error = @driver.find_element(ERROR_BANNER)
        error_text = error.text
    end

end