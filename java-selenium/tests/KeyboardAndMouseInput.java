import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;

public class KeyboardAndMouseInput {
    public static void main(String[] args) {

        // System.setProperty("webdriver.chrome.driver", "/Users/meaghanlewis/Downloads/chromedriver");

        WebDriver driver = new ChromeDriver();

        driver.get("https://formy-project.herokuapp.com/keypress");

        // Find input field
        WebElement name = driver.findElement(By.id("name"));
        // Click input field
        name.click();
        // Enter text
        name.sendKeys("Kurt");

        // Find button
        WebElement button = driver.findElement(By.id("button"));
        // Click button
        button.click();

        driver.quit();
    }
}
